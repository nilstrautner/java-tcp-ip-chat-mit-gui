import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import socketio.*;

public class Client_GUI extends JFrame implements ActionListener {
  
  JTextArea jta;
  JButton jbut;
  Socket client;
  JTextField jtf1, jtf2;
  String nick = "";
  
  public Client_GUI () {
    //Frame
    super("Chat V2 - nicht eingeloggt");
    setSize(500, 400);
    setVisible(true);
    //setResizable(false);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setLayout(new FlowLayout());
    
    //Label
    JLabel l1 = new JLabel("Chat:");
    l1.setLocation(10, 10);
    add(l1);
    
    //JTextArea
    jta = new JTextArea(10, 40);
    jta.setBounds(10, 30, 200, 400);
    //jta.setLineWrap(false);
    jta.setEditable(false);
    jta.setLineWrap(true);
    jta.setWrapStyleWord(true);
    jta.setPreferredSize(new Dimension(350, 300));
    add(jta);
    //Scroll
    JScrollPane scrollPane = new JScrollPane(jta);
    add(scrollPane);
    
    //Jtextfield
    jtf1 = new JTextField(7);
    add(jtf1);
    
    //Jtextfield
    jtf2 = new JTextField(30);
    jtf2.setEnabled(false);
    jtf2.setText("Nutzername eingeben");
    add(jtf2);
    
    //JButton
    jbut = new JButton("Senden");
    jbut.addActionListener(this);
    add(jbut);
    
    try {
      client = new Socket("127.0.0.1", 1234);
      client.connect();
    } catch(Exception e) {
      System.out.println("Fehler 0" + e.getMessage());
    } 

    Thread th2 = new Thread(new Empfanger(client, this));
    th2.start();
  }
  
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == jbut) {
      try {
        if (nick.equals("")) {
          //1. Mal, Username eingeben
          nick = jtf1.getText();
          client.write(nick + "\n");
          jtf1.setEnabled(false);
          setTitle("Chat V2 - eingeloggt als " + nick);
          jtf2.setEnabled(true);
          jtf2.setText("");
        } else {
          String temp = jtf2.getText();
          if (temp.charAt(0) == '/') {
            commands(temp);
            return;
          }
          client.write(nick + ": " + temp + "\n");
          jtf2.setText("");
        } // end of if-else
      } catch(Exception ex) {
        System.out.println("Fehler 1: " + ex.getMessage());
      }
    }
  }
  
  public void commands(String temp) {
    if (temp.contains("help")) {
      jta.setText(jta.getText() + "\n" + "##### Commands #####");
      jta.setText(jta.getText() + "\n" + " /show \n /rename <Name>");
      jta.setText(jta.getText() + "\n" + "##### End-Commands #####");
      return;
    }
    if (temp.contains("show")) {
      
    }
  }
  
  public static void main(String[] args) {
    new Client_GUI();
  } // end of main
  
  public class Empfanger implements Runnable {
    Socket client;
    boolean open = true;
    Client_GUI cgui;
    
    
    public Empfanger(Socket client, Client_GUI cgui) {
      this.client = client;
      this.cgui = cgui;
    }
    
    public void run() {
      while (open) { 
        try {
          while (client.dataAvailable() <= 0);
          String inhalt = client.readLine();
          cgui.jta.setText(cgui.jta.getText() + "\n" + inhalt);
        } catch(Exception e) {
          System.out.println("Fehler: " + e.getMessage());
        } 
      } // end of while
    }
  }

} // end of class Client_GUI

